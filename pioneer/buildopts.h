#ifndef BUILDOPTS_H
#define BUILDOPTS_H

#define PIONEER_EXTRAVERSION ""
#define PIONEER_VERSION "20190723"
#define PIONEER_DATA_DIR "/usr/share/pioneer"

#define WITH_OBJECTVIEWER 1
#define WITH_DEVKEYS 1

#endif /* BUILDOPTS_H */
